Java基于百度AI+JavaCV+OpenCV 实现摄像头人数动态统计 
-------------------------------------------------------------------------------
### 运行Demo

在JavavcCameraTest代码中修改以下内容运行即可。

修改ACCESS_TOKEN的值为自己百度应用apikey secretkey获取的哦。具体如何获取看百度AI文档。图文教程哦很简单

修改140、168行图片存的本地路径即可

-------------------------------------------------------------------------------
#### 百度AI官网:[https://ai.baidu.com/](https://ai.baidu.com/)

#### 人流量统计-动态版接口文档：[https://ai.baidu.com/docs#/Body-API/4030d2cd](https://ai.baidu.com/docs#/Body-API/4030d2cd)

#### 注意:接口是内测阶段。需要使用的请自己去百度申请哦。

-------------------------------------------------------------------------------

此demo工程使用MyEclipse搭建。jar管理使用Maven


下载的jar很多。请确保自己的Maven默认下载地址是阿里云的哦。

POM内容配置如下。
```
 <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <ffmpeg.version>3.2.1-1.3</ffmpeg.version>
    <javacv.version>1.4.1</javacv.version>
  </properties>

  <dependencies>
 
 <dependency>
      <groupId>org.bytedeco.javacpp-presets</groupId>
      <artifactId>ffmpeg-platform</artifactId>
      <version>${ffmpeg.version}</version>
    </dependency>
    <!-- fastjson -->
    <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>fastjson</artifactId>
        <version>1.2.35</version>
    </dependency>
    <dependency>
      <groupId>org.bytedeco</groupId>
      <artifactId>javacv</artifactId>
      <version>${javacv.version}</version>
    </dependency>

    <dependency>
        <groupId>org.bytedeco.javacpp-presets</groupId>
        <artifactId>opencv-platform</artifactId>
        <version>3.4.1-1.4.1</version>
    </dependency>
  </dependencies>
```
